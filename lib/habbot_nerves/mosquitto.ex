defmodule HabbotNerves.Mosquitto do
  @moduledoc "Erlang Port for Mosquitto server"

  use GenServer, start: {__MODULE__, :start_link, []}


  def start_link() do
    GenServer.start_link(__MODULE__, nil, name: __MODULE__)
  end


  def init(_) do
    port = Port.open({:spawn_executable, "/usr/sbin/mosquitto"}, [:binary])
    {:ok, %{port: port}}
  end

end
